package eu.eukleia;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * The main class to lanch the application.
 */
@SpringBootApplication
public class TinyLibraryApplication {

	/**
	 * The main method. Launch the application
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		SpringApplication.run(TinyLibraryApplication.class, args);
	}

}
