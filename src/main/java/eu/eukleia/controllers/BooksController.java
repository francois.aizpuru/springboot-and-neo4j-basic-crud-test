package eu.eukleia.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import eu.eukleia.model.Author;
import eu.eukleia.model.Book;
import eu.eukleia.repository.AuthorRepository;
import eu.eukleia.repository.BookRepository;

/**
 * The Class BooksController.
 */
@RestController
public class BooksController {

	/** The book repository. */
	@Autowired
	private BookRepository bookRepository;

	/** The author repository. */
	@Autowired
	private AuthorRepository authorRepository;

	/**
	 * Index.
	 *
	 * @return the welcoming message
	 */
	@RequestMapping("/")
	public String index() {
		return "Hello eukleia";
	}

	/**
	 * Gets the by author.
	 *
	 * @param author the author id
	 * @return the list of books written by this author
	 */
	@RequestMapping("/books")
	public ResponseEntity<Iterable<Book>> getByAuthor(@Param("author") Optional<Long> author) {
		Iterable<Book> result;
		if (author.isPresent()) {
			result = bookRepository.findByAuthor(author.get());
		} else {
			result = bookRepository.findAll();
		}
		return new ResponseEntity<Iterable<Book>>(result, HttpStatus.OK);

	}

	/**
	 * Gets the by id.
	 *
	 * @param id the id of a book
	 * @return the book entity with such an id
	 */
	@RequestMapping("/books/{id}")
	public ResponseEntity<Book> getById(@PathVariable("id") long id) {
		Optional<Book> result = bookRepository.findById(id);
		if (result.isPresent()) {
			return new ResponseEntity<Book>(result.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<Book>(HttpStatus.NOT_FOUND);
		}
	}

	/**
	 * Search book.
	 *
	 * @param search the search value The search is case sensitive and limited to -
	 *               title - reference - release date - author firstname - author
	 *               lastname
	 * @return the books selected by the search
	 */
	@RequestMapping("/books/search/{search}")
	public ResponseEntity<List<Book>> searchBook(@PathVariable("search") String search) {
		List<Book> result = bookRepository.searchBook(search);
		return new ResponseEntity<List<Book>>(result, HttpStatus.OK);
	}

	/**
	 * Creates the book.
	 *
	 * @param book the book to create
	 * @return the created book entity
	 */
	@PostMapping(path = "/books", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Book> create(@RequestBody Book book) {
		if(book.getId() !=null) {
			// do not provide id in case of create
			return new ResponseEntity<Book>(HttpStatus.NOT_ACCEPTABLE);
		}
		return createOrReplace(book);
	}

	/**
	 * 
	 * @param book the book to create or update
	 * Author Id is used to make the link with an already existing author
	 * @return the created/updated book
	 */
	private ResponseEntity<Book> createOrReplace(Book book) {
		Author author = book.getAuthor();
		if (author != null && author.getId() != null) {
			author = authorRepository.findById(author.getId()).orElse(null);
			if (author == null) {
				return new ResponseEntity<Book>(HttpStatus.NOT_FOUND);
			}
			book.setAuthor(author);
		}
		Book created = bookRepository.save(book);
		return new ResponseEntity<Book>(created, HttpStatus.CREATED);
	}

	/**
	 * 
	 * @param book the book to replace
	 * @return the updated book
	 */
	@PutMapping(path = "/books", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Book> replace(@RequestBody Book book) {
		Optional<Book> saved = bookRepository.findById(book.getId());
		if (!saved.isPresent()) {
			return new ResponseEntity<Book>(HttpStatus.NOT_FOUND);
		} else {
			return createOrReplace(book);
		}
	}

}
