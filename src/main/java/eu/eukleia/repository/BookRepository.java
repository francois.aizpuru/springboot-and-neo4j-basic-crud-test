package eu.eukleia.repository;

import java.util.List;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;

import eu.eukleia.model.Book;

/**
 * The BookRepository to query neo4j.
 */
public interface BookRepository extends Neo4jRepository<Book, Long> {

	/**
	 * Find by author.
	 *
	 * @param id the id
	 * @return the list
	 */
	@Query("MATCH (book:Book)-[r:AUTHORED]->(author:Author) "//
			+ "WHERE ID(author)=$id RETURN book,author,r")
	List<Book> findByAuthor(Long id);

	/**
	 * Search book.
	 *
	 * @param search the search query  
	 * The search is case sensitive and limited to
	 * - title 
	 * - reference 
	 * - release date 
	 * - author firstname 
	 * - author lastname
	 * @return the list of books finded
	 */
	@Query("MATCH (book:Book) " + "OPTIONAL MATCH (book)-[r:AUTHORED]->(author:Author) " //
			+ "WHERE book.title CONTAINS $search "//
			+ "OR book.reference CONTAINS $search " //
			+ "OR book.releaseDate CONTAINS $search "//
			+ "OR author.firstName CONTAINS $search " //
			+ "OR author.lastName CONTAINS $search " //
			+ "RETURN book,r,author")
	List<Book> searchBook(String search);

}
