package eu.eukleia.repository;

import org.springframework.data.neo4j.repository.Neo4jRepository;

import eu.eukleia.model.Author;

/**
 * The AuthorRepository to query neo4j
 */
public interface AuthorRepository extends Neo4jRepository<Author, Long> {

}
