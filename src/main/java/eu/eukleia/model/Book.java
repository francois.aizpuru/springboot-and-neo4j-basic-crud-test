package eu.eukleia.model;

import java.util.Date;

import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.Relationship;

/**
 * The Class Book.
 */
public class Book {

	/** The id. */
	@Id
	@GeneratedValue
	private Long id;

	/** The title. */
	private String title;
	
	/** The reference. */
	private String reference;
	
	/** The release date. */
	private Date releaseDate;
	
	/** The about. */
	private String about;


	/** The author. */
	@Relationship(type = "AUTHORED", direction = Relationship.OUTGOING)
	private Author author;

	/**
	 * Gets the author.
	 *
	 * @return the author
	 */
	public Author getAuthor() {
		return author;
	}

	/**
	 * Sets the author.
	 *
	 * @param author the new author
	 */
	public void setAuthor(Author author) {
		this.author = author;
	}

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the reference.
	 *
	 * @return the reference
	 */
	public String getReference() {
		return reference;
	}

	/**
	 * Sets the reference.
	 *
	 * @param reference the new reference
	 */
	public void setReference(String reference) {
		this.reference = reference;
	}

	/**
	 * Gets the release date.
	 *
	 * @return the release date
	 */
	public Date getReleaseDate() {
		return releaseDate;
	}

	/**
	 * Sets the release date.
	 *
	 * @param releaseDate the new release date
	 */
	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}

	/**
	 * Gets the about.
	 *
	 * @return the about
	 */
	public String getAbout() {
		return about;
	}

	/**
	 * Sets the about.
	 *
	 * @param about the new about
	 */
	public void setAbout(String about) {
		this.about = about;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

}
