# Tiny Library
CRUD Rest API based on neo4j

## Start
`./mvnw spring-boot:run`
\
neo4j credentials can be changed in this file:
`src/main/resources/application.properties`

## Get all the books
`GET localhost:8080/books`

## Get all the books by author
`GET localhost:8080/books?author=${id}`

## Get a book by id
`GET localhost:8080/books/${id}`

## Search books
`GET localhost:8080/books/search/${query}`

## Create books
`POST localhost:8080/books`
\
body to create the author too: 
```json
{
    "title": "${title}",
    "reference": "$reference",
    "about": "${about}",
    "releaseDate": "${releaseDate}",
    "author": {
        "firstName": "${firstName}",
        "lastName": "${lastName}"
    }
}
```

body to link it to an existing author: 
```json
{
    "title": "${title}",
    "reference": "$reference",
    "about": "${about}",
    "releaseDate": "${releaseDate}",
    "author": {
        "id": "${id}",
    }
}
```
### Update Books
`PUT localhost:8080/books`
\
body to create an author too: 
```json
{
    "id": "${id}",
    "title": "${title}",
    "reference": "$reference",
    "about": "${about}",
    "releaseDate": "${releaseDate}",
    "author": {
        "firstName": "${firstName}",
        "lastName": "${lastName}"
    }
}
```
body to link it to an existing author: 

```json
{
    "id": "${id}",
    "title": "${title}",
    "reference": "$reference",
    "about": "${about}",
    "releaseDate": "${releaseDate}",
    "author": {
        "id": "${id}",
    }
}
```